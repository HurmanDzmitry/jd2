package by.hurman_dzmitry.config.core;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("classpath:dataSource.properties")
public class DatabaseConfig {

    @Value("${driverClassName}")
    private String driverClassName;

    @Value("${jdbcUrl}")
    private String jdbcUrl;

    @Bean(value = "dataSource", destroyMethod = "close")
    @Scope("singleton")
    public BasicDataSource getDatasource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(jdbcUrl);
        return dataSource;
    }
}