package by.hurman_dzmitry;

import by.hurman_dzmitry.config.core.DatabaseConfig;
import by.hurman_dzmitry.config.core.JdbcTemplateConfig;
import by.hurman_dzmitry.config.swagger.SwaggerConfig;
import by.hurman_dzmitry.config.web.WebSecurityConfiguration;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.util.Properties;

@EnableSwagger2
@EnableAspectJAutoProxy
@EnableTransactionManagement(proxyTargetClass = true)
@SpringBootApplication(
    scanBasePackages = {"by.hurman_dzmitry"},
    exclude = {JacksonAutoConfiguration.class, ErrorMvcAutoConfiguration.class})
@Import({
  DatabaseConfig.class,
  JdbcTemplateConfig.class,
  SwaggerConfig.class,
  WebSecurityConfiguration.class
})
public class ApplicationStarter extends SpringBootServletInitializer {

  @Autowired private Environment env;

  public static void main(String[] args) {
    SpringApplication.run(ApplicationStarter.class, args);
  }

  @Autowired
  @Bean(name = "sessionFactory")
  public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
    LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
    factoryBean.setPackagesToScan("by.hurman_dzmitry");
    factoryBean.setDataSource(dataSource);
    factoryBean.setHibernateProperties(getAdditionalProperties());
    factoryBean.afterPropertiesSet();
    SessionFactory sf = factoryBean.getObject();
    return sf;
  }

  @Autowired
  @Bean(name = "entityManagerFactory")
  @Primary
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean em
            = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource);
    em.setPackagesToScan("by.hurman_dzmitry");
    JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    em.setJpaProperties(getAdditionalProperties());
    return em;
  }

  private Properties getAdditionalProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    properties.put("hibernate.show_sql", "true");
    properties.put(
        " current_session_context_class",
        "org.springframework.orm.hibernate5.SpringSessionContext");
    return properties;
  }
}
