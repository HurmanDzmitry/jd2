package by.hurman_dzmitry.repository;

import java.util.List;

public interface GenericDao<T, K> {

  List<T> findAll();

  T findById(K id);

  void delete(K id);

  T add(T entity);

  T update(T entity);
}
