package by.hurman_dzmitry.repository.customer;

import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.domain.Customer_;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Qualifier("customerDaoImpl")
public class CustomerDaoImpl implements CustomerDao {

  @Autowired
  @Qualifier("sessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public List<Customer> findAll() {
    try (Session session = sessionFactory.openSession()) {
      return session.createQuery("select tu from Customer tu", Customer.class).getResultList();
    }
  }

  @Override
  public Customer findById(Long id) {
    try (Session session = sessionFactory.openSession()) {
      return session.find(Customer.class, id);
    }
  }

  @Override
  public void delete(Long id) {
    try (Session session = sessionFactory.openSession()) {
      session.remove(findById(id));
    }
  }

  @Override
  public Customer add(Customer entity) {
    try (Session session = sessionFactory.openSession()) {
      Transaction transaction = session.getTransaction();
      transaction.begin();
      Long newcustomerID = (Long) session.save(entity);
      transaction.commit();
      return session.find(Customer.class, newcustomerID);
    }
  }

  @Override
  public Customer update(Customer entity) {
    try (Session session = sessionFactory.openSession()) {
      Transaction transaction = session.getTransaction();
      transaction.begin();
      session.saveOrUpdate(entity);
      transaction.commit();
      return session.find(Customer.class, entity.getCustomerID());
    }
  }

  @Override
  public List<Customer> search(String searchQuery, Integer limit, Integer offset) {
    CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
    CriteriaQuery<Customer> query = cb.createQuery(Customer.class);
    Root<Customer> root = query.from(Customer.class);
    ParameterExpression<String> param = cb.parameter(String.class);
    query
        .select(root)
        .distinct(true)
        .where(
            cb.or(
                cb.like(root.get(Customer_.name), param),
                cb.like(root.get(Customer_.surname), param)));

    try (Session session = sessionFactory.openSession()) {
      TypedQuery<Customer> resultQuery = session.createQuery(query);
      resultQuery.setParameter(param, searchQuery);
      resultQuery.setMaxResults(limit);
      resultQuery.setFirstResult(offset);
      return resultQuery.getResultList();
    }
  }
}
