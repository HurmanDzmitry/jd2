package by.hurman_dzmitry.repository.stock;

import by.hurman_dzmitry.domain.Stock;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Qualifier("StockDaoImpl")
public class StockDaoImpl implements StockDao {

  @Autowired
  @Qualifier("sessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public List<Stock> findAll() {
    try (Session session = sessionFactory.openSession()) {
      return session.createQuery("select tu from Stock tu", Stock.class).getResultList();
    }
  }

  @Override
  public Stock findById(Long id) {
    try (Session session = sessionFactory.openSession()) {
      return session.find(Stock.class, id);
    }
  }

  @Override
  public void delete(Long id) {

  }

  public void delete(Stock stock) {
    try (Session session = sessionFactory.openSession()) {
      session.remove(stock);
    }
  }

  @Override
  public Stock add(Stock entity) {
    try (Session session = sessionFactory.openSession()) {
      Transaction transaction = session.getTransaction();
      transaction.begin();
      Long newStockID = (Long) session.save(entity);
      transaction.commit();
      return session.find(Stock.class, newStockID);
    }
  }

  @Override
  public Stock update(Stock entity) {
    try (Session session = sessionFactory.openSession()) {
      Transaction transaction = session.getTransaction();
      transaction.begin();
      session.saveOrUpdate(entity);
      transaction.commit();
      return session.find(Stock.class, entity.getStockID());
    }
  }

  @Override
  public List<Stock> search(String searchQuery) {
    CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
    CriteriaQuery<Stock> query = cb.createQuery(Stock.class);
    Root<Stock> root = query.from(Stock.class);
    ParameterExpression<String> param = cb.parameter(String.class);
    query.select(root)
            .distinct(true)
            .where(
                    cb.or(
                            cb.like(root.get("name"), param)
                    )
            ).orderBy(cb.asc(root.get("name")));

    try (Session session = sessionFactory.openSession()) {
      TypedQuery<Stock> resultQuery = session.createQuery(query);
      resultQuery.setParameter(param, searchQuery);
      return resultQuery.getResultList();
    }
  }
}
