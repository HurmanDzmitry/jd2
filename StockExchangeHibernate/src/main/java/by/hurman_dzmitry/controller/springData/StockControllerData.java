package by.hurman_dzmitry.controller.springData;

import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.repository.stock.StockData;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/springData/stocks")
public class StockControllerData {

  @Autowired private StockData stockData;

  @ApiOperation(value = "Show all stocks")
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Page<Stock>> getStocks(@ApiIgnore Pageable pageable) {
    return new ResponseEntity<>(stockData.findAll(pageable), HttpStatus.OK);
  }

  @ApiOperation(value = "Search stock by id")
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Optional<Stock>> getStockById(@PathVariable Long id) {
    return new ResponseEntity<>(stockData.findById(id), HttpStatus.OK);
  }

  @ApiOperation(value = "New stock")
  @PostMapping("/create")
  @Transactional
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Stock> createStock(@RequestBody Stock requestStock) {
    Stock stock = new Stock();
    stock.setName(requestStock.getName());
    stock.setBid(requestStock.getBid());
    stock.setNumberFreeStock(requestStock.getNumberFreeStock());
    return new ResponseEntity<>(stockData.save(stock), HttpStatus.OK);
  }

  @ApiOperation(value = "Update Stock by id")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Successful Stock update"),
    @ApiResponse(code = 400, message = "Invalid Stock id supplied"),
    @ApiResponse(code = 404, message = "Stock was not found"),
    @ApiResponse(code = 500, message = "Server error, something wrong")
  })
  @PutMapping("/update/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Stock> updateStock(
      @PathVariable("id") Long id, @RequestBody Stock request) {
    Optional<Stock> stockOptional = stockData.findById(id);
    Stock stock = stockOptional.get();
    stock.setName(request.getName());
    stock.setBid(request.getBid());
    stock.setNumberFreeStock(request.getNumberFreeStock());
    return new ResponseEntity<>(stockData.save(stock), HttpStatus.OK);
  }

  @ApiOperation(value = "Search Stock by query")
  @GetMapping("/search")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Stock>> searchStocks(String search) {
    return new ResponseEntity<>(stockData.findByName(search), HttpStatus.OK);
  }

  @ApiOperation(value = "Delete Stock by id")
  @DeleteMapping("/delete/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Long> deleteStock(@PathVariable("id") Long id) {
    Optional<Stock> stockOptional = stockData.findById(id);
    Stock stock = stockOptional.get();
    stockData.delete(stock);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }
}
