create table stocks_customers
(
  id_stock     int         not null,
  id_customer  int         not null,
  constraint stocks_customers_customers_id_fk
    foreign key (id_customer) references customers (id_customer)
      on update cascade on delete cascade,
  constraint stocks_customers_stocks_id_stock_fk
    foreign key (id_stock) references stocks (id_stock)
      on update cascade on delete cascade,
  number_bought_stock int not null
);
