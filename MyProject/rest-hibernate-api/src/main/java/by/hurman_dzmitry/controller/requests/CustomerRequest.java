package by.hurman_dzmitry.controller.requests;

import by.hurman_dzmitry.domain.Stock;

import java.util.Objects;
import java.util.Set;

public class CustomerRequest {

  private String name;
  private String surname;
  private Set<Stock> stocks;

  public CustomerRequest() {}

  public CustomerRequest(String name, String surname, Set<Stock> stocks) {
    this.name = name;
    this.surname = surname;
    this.stocks = stocks;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

    public Set<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerRequest that = (CustomerRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(stocks, that.stocks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, stocks);
    }

    @Override
    public String toString() {
        return "CustomerRequest{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", stocks=" + stocks +
                '}';
    }
}