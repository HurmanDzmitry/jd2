package by.hurman_dzmitry.controller.hibernate;

import by.hurman_dzmitry.controller.requests.CustomerRequest;
import by.hurman_dzmitry.controller.requests.SearchCriteria;
import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.repository.customer.CustomerDao;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/hibernate/customers")
public class CustomerController {

  @Autowired private CustomerDao customerDao;

  @ApiOperation(value = "Show all customers")
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Customer>> getCustomers() {
    return new ResponseEntity<>(customerDao.findAll(), HttpStatus.OK);
  }

  @ApiOperation(value = "Search customer by id")
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
    Customer customer = customerDao.findById(id);
    return new ResponseEntity<>(customer, HttpStatus.OK);
  }

  @ApiOperation(value = "New customer")
  @PostMapping("/create")
  @Transactional
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Customer> createCustomer(@RequestBody Customer request) {
    Customer customer = new Customer();
    customer.setName(request.getName());
    customer.setSurname(request.getSurname());
    customer.setLogin(request.getLogin());
    customer.setPassword(request.getPassword());
    customer.setStocks(request.getStocks());
    return new ResponseEntity<>(customerDao.add(customer), HttpStatus.OK);
  }

  @ApiOperation(value = "Update customer by id")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Successful customer update"),
    @ApiResponse(code = 400, message = "Invalid customer id supplied"),
    @ApiResponse(code = 404, message = "Customer was not found"),
    @ApiResponse(code = 500, message = "Server error, something wrong")
  })
  @PutMapping("/update/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Customer> updateCustomer(
      @PathVariable("id") Long id, @RequestBody CustomerRequest request) {
    Customer customer = customerDao.findById(id);
    customer.setName(request.getName());
    customer.setSurname(request.getSurname());
    customer.setLogin(customer.getLogin());
    customer.setPassword(customer.getPassword());
    customer.setStocks(request.getStocks());
    return new ResponseEntity<>(customerDao.update(customer), HttpStatus.OK);
  }

  @ApiOperation(value = "Search customer by query")
  @GetMapping("/search")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Customer>> searchStocks(SearchCriteria search) {
    return new ResponseEntity<>(
        customerDao.search(search.getQuery(), search.getLimit(), search.getOffset()),
        HttpStatus.OK);
  }

  @ApiOperation(value = "Delete customer by id")
  @DeleteMapping("/delete/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Long> deleteCustomer(@PathVariable("id") Long id) {
    customerDao.delete(id);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }
}
