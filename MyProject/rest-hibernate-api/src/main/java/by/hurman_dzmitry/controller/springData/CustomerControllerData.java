package by.hurman_dzmitry.controller.springData;

import by.hurman_dzmitry.controller.requests.CustomerRequest;
import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.repository.customer.CustomerData;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/springData/customers")
public class CustomerControllerData {

  @Autowired private CustomerData customerData;

  @ApiOperation(value = "Show all customers")
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Page<Customer>> getCustomers(@ApiIgnore Pageable pageable) {
    return new ResponseEntity<>(customerData.findAll(pageable), HttpStatus.OK);
  }

  @ApiOperation(value = "Search customer by id")
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Optional<Customer>> getCustomerById(@PathVariable Long id) {
    return new ResponseEntity<>(customerData.findById(id), HttpStatus.OK);
  }

  @ApiOperation(value = "New customer")
  @PostMapping("/create")
  @Transactional
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Customer> createCustomer(
      @RequestBody Customer requestCustomer/*, Stock requestStock*/) {
    Customer customer = new Customer();
    customer.setName(requestCustomer.getName());
    customer.setSurname(requestCustomer.getSurname());
    customer.setLogin(requestCustomer.getLogin());
    customer.setPassword(requestCustomer.getPassword());
//    Set<Stock> stockSet = Collections.emptySet();
//    for (Stock st : requestCustomer.getStocks()) {
//      st.setName(requestStock.getName());
//      st.setBid(requestStock.getBid());
//      st.setNumberFreeStock(requestStock.getNumberFreeStock());
//      stockSet.add(st);
//    }
//    customer.setStocks(stockSet);
    return new ResponseEntity<>(customerData.save(customer), HttpStatus.OK);
  }

  @ApiOperation(value = "Update customer by id")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Successful customer update"),
    @ApiResponse(code = 400, message = "Invalid customer id supplied"),
    @ApiResponse(code = 404, message = "Customer was not found"),
    @ApiResponse(code = 500, message = "Server error, something wrong")
  })
  @PutMapping("/update/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Customer> updateCustomer(
      @PathVariable("id") Long id, @RequestBody CustomerRequest request) {
    Optional<Customer> customerOptional = customerData.findById(id);
    Customer customer = customerOptional.get();
    customer.setName(request.getName());
    customer.setSurname(request.getSurname());
    customer.setLogin(customer.getLogin());
    customer.setPassword(customer.getPassword());
    customer.setStocks(request.getStocks());
    return new ResponseEntity<>(customerData.save(customer), HttpStatus.OK);
  }

  @ApiOperation(value = "Search customer by query")
  @GetMapping("/search")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Customer>> searchCustomers(String search) {
    return new ResponseEntity<>(customerData.findByNameOrSurname(search), HttpStatus.OK);
  }

  @ApiOperation(value = "Delete customer by id")
  @DeleteMapping("/delete/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Long> deleteCustomer(@PathVariable("id") Long id) {
    Optional<Customer> customerOptional = customerData.findById(id);
    Customer customer = customerOptional.get();
    customerData.delete(customer);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }
}
