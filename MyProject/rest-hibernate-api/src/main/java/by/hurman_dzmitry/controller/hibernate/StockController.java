package by.hurman_dzmitry.controller.hibernate;

import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.repository.stock.StockDao;
import by.hurman_dzmitry.repository.stock.StockDaoImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/hibernate/stocks")
public class StockController {

  @Autowired private StockDao stockDao;
  @Autowired private StockDaoImpl stockDaoImp;

  @ApiOperation(value = "Show all stocks")
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Stock>> getStocks() {
    return new ResponseEntity<>(stockDao.findAll(), HttpStatus.OK);
  }

  @ApiOperation(value = "Search stock by id")
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Stock> getStockById(@PathVariable Long id) {
    Stock stock = stockDao.findById(id);
    return new ResponseEntity<>(stock, HttpStatus.OK);
  }

  @ApiOperation(value = "New stock")
  @PostMapping("/create")
  @Transactional
  @ResponseStatus(HttpStatus.CREATED)
  public ResponseEntity<Stock> createStock(@RequestBody Stock request) {
    Stock stock = new Stock();
    stock.setName(request.getName());
    stock.setBid(request.getBid());
    stock.setNumberFreeStock(request.getNumberFreeStock());
    return new ResponseEntity<>(stockDao.add(stock), HttpStatus.OK);
  }

  @ApiOperation(value = "Update stock by id")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Successful stock update"),
    @ApiResponse(code = 400, message = "Invalid stock id supplied"),
    @ApiResponse(code = 404, message = "Stock was not found"),
    @ApiResponse(code = 500, message = "Server error, something wrong")
  })
  @PutMapping("/update/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Stock> updateStock(
      @PathVariable("id") Long id, @RequestBody Stock request) {
    Stock stock = stockDao.findById(id);
    stock.setName(request.getName());
    stock.setBid(request.getBid());
    stock.setNumberFreeStock(request.getNumberFreeStock());
    return new ResponseEntity<>(stockDao.update(stock), HttpStatus.OK);
  }

  @ApiOperation(value = "Search stock by query")
  @GetMapping("/search")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Stock>> searchStocks(String query) {
    return new ResponseEntity<>(stockDao.search(query), HttpStatus.OK);
  }

  @ApiOperation(value = "Delete stock by id")
  @DeleteMapping("/delete/{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Long> deleteStock(@PathVariable("id") Long id) {
    Stock stock=stockDaoImp.findById(id);
    stockDaoImp.delete(stock);
    return new ResponseEntity<>(id, HttpStatus.OK);
  }
}