package by.hurman_dzmitry.service;

import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.domain.Role;
import by.hurman_dzmitry.repository.customer.CustomerDao;
import by.hurman_dzmitry.repository.role.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private RoleDao roleDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Customer customer = customerDao.findByLogin(username);
            List<Role> roles = roleDao.getRolesByCustomerId(customer.getCustomerID());
            if(customer.getCustomerID() == null){
                throw new UsernameNotFoundException(String.format("No customer found with username '%s'.", username));
            } else {
                return new org.springframework.security.core.userdetails.User(
                        customer.getLogin(),
                        customer.getPassword(),
                        AuthorityUtils.commaSeparatedStringToAuthorityList(roles.get(0).getRoleName())
                );
            }
        } catch (Exception e) {
            throw new UsernameNotFoundException("customer with this login not found");
        }
    }
}