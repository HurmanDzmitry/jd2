package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.controller.requests.CustomerCreateRequest;
import by.hurman_dzmitry.controller.requests.SearchCriteria;
import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.domain.Role;
import by.hurman_dzmitry.repository.customer.CustomerDao;
import by.hurman_dzmitry.repository.role.RoleDao;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest/customers")
public class CustomerController {

    private final CustomerDao customerDao;

    private final RoleDao roleDao;

    @Autowired
    public CustomerController(CustomerDao customerDao, RoleDao roleDao) {
        this.customerDao = customerDao;
        this.roleDao = roleDao;
    }

    @ApiOperation(value = "Show all customers")
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Customer>> getCustomers() {
        return new ResponseEntity<>(customerDao.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Search customer by id")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Customer> getCustomerById(@PathVariable Long id) {
        Customer customer = customerDao.findById(id);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @ApiOperation(value = "New customer")
    @PostMapping("/signUp")
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer request) {
        Customer customer = new Customer();
        customer.setName(request.getName());
        customer.setSurname(request.getSurname());
        customer.setStockID(request.getStockID());
        customer.setNumberBoughtStock(request.getNumberBoughtStock());
        customer.setLogin(request.getLogin());
        customer.setPassword(request.getPassword());
        Customer savedCustomer = customerDao.add(customer);
        roleDao.add(new Role(savedCustomer.getCustomerID(), "ROLE_USER"));
        return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
    }

    @ApiOperation(value = "Update customer by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful customer update"),
            @ApiResponse(code = 400, message = "Invalid customer id supplied"),
            @ApiResponse(code = 404, message = "customer was not found"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @PutMapping("/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Customer> updateCustomer(@PathVariable("id") Long id,
                                                   @RequestBody CustomerCreateRequest request) {
        Customer customer = customerDao.findById(id);
        customer.setName(request.getName());
        customer.setSurname(request.getSurname());
        customer.setStockID(request.getStockID());
        customer.setNumberBoughtStock(request.getNumberBoughtStock());
        customer.setLogin(customer.getLogin());
        customer.setPassword(customer.getPassword());
        return new ResponseEntity<>(customerDao.update(customer), HttpStatus.OK);
    }

    @ApiOperation(value = "Search customer by query")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "limit", value = "limit of users", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "offset", value = "start node of users", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "query", value = "search query", required = true, dataType = "string", paramType = "query"),
    })
    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Customer>> searchCustomers(@ApiIgnore @ModelAttribute SearchCriteria search) {
        List<Customer> searchResult =
                customerDao.search(
                        search.getQuery(),
                        search.getLimit(),
                        search.getOffset()
                );
        return new ResponseEntity<>(searchResult, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete customer by id")
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Long> deleteCustomer(@PathVariable("id") Long id) {
        customerDao.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}