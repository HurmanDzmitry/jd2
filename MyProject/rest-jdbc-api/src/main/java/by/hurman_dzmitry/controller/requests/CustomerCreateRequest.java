package by.hurman_dzmitry.controller.requests;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

public class CustomerCreateRequest {

    private String name;
    private String surname;
    private Long stockID;
    private Long numberBoughtStock;

    public CustomerCreateRequest() {
    }

    public CustomerCreateRequest(String name, String surname, Long stockID, Long numberBoughtStock) {
        this.name = name;
        this.surname = surname;
        this.stockID = stockID;
        this.numberBoughtStock = numberBoughtStock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public Long getNumberBoughtStock() {
        return numberBoughtStock;
    }

    public void setNumberBoughtStock(Long numberBoughtStock) {
        this.numberBoughtStock = numberBoughtStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerCreateRequest that = (CustomerCreateRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(stockID, that.stockID) &&
                Objects.equals(numberBoughtStock, that.numberBoughtStock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, stockID, numberBoughtStock);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
