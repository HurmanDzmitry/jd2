package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.domain.Role;
import by.hurman_dzmitry.repository.role.RoleDao;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/rest/roles")
public class RoleController {
    @Autowired
    private RoleDao roleDao;

    @ApiOperation(value = "Show all roles")
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<Role>> getRoles() {
        return new ResponseEntity<>(roleDao.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Search role by id")
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Role> getRoleById(@PathVariable Long id) {
        Role role = roleDao.findById(id);
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @ApiOperation(value = "New role")
    @PostMapping("/create")
    @Transactional
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Role> createRole(@RequestBody Role request) {
        Role role = new Role();
        role.setCustomerId(request.getCustomerId());
        role.setRoleName(request.getRoleName());
        return new ResponseEntity<>(roleDao.add(role), HttpStatus.OK);
    }

    @ApiOperation(value = "Update role by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Successful role update"),
            @ApiResponse(code = 400, message = "Invalid role id supplied"),
            @ApiResponse(code = 404, message = "Role was not found"),
            @ApiResponse(code = 500, message = "Server error, something wrong")
    })
    @PutMapping("/update/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Role> updateRole(@PathVariable("id") Long id,
                                           @RequestBody Role request) {
        Role role = roleDao.findById(id);
        role.setCustomerId(request.getCustomerId());
        role.setRoleName(request.getRoleName());
        return new ResponseEntity<>(roleDao.update(role), HttpStatus.OK);
    }

    @ApiOperation(value = "Delete role by id")
    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Long> deleteRole(@PathVariable("id") Long id) {
        roleDao.delete(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}