package by.hurman_dzmitry.repository.stock;

import by.hurman_dzmitry.domain.Stock;
import by.hurman_dzmitry.repository.GenericDao;

import java.util.List;

public interface StockDao extends GenericDao<Stock, Long> {
  List<Stock> search(String query);
}
