package by.hurman_dzmitry.repository.stock;

import by.hurman_dzmitry.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StockData
    extends JpaRepository<Stock, Long>,
        CrudRepository<Stock, Long>,
        PagingAndSortingRepository<Stock, Long> {

  @Query("select u from Stock u where lower(u.name) LIKE lower(:search)")
  List<Stock> findByName(String search);
}
