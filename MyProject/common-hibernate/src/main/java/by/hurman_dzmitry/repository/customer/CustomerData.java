package by.hurman_dzmitry.repository.customer;

import by.hurman_dzmitry.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CustomerData
    extends JpaRepository<Customer, Long>,
        CrudRepository<Customer, Long>,
        PagingAndSortingRepository<Customer, Long> {

  @Query(
      "select u from Customer u where lower(u.name) LIKE lower(:search) or lower(u.surname) LIKE lower(:search)")
  List<Customer> findByNameOrSurname(String search);
}
