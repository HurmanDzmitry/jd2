package by.hurman_dzmitry.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "customers")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "customerID")
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_customer")
  private Long customerID;

  @Column(name = "name")
  private String name;

  @Column(name = "surname")
  private String surname;

  @Column(name = "login")
  private String login;

  @Column(name = "password")
  private String password;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(
      name = "stocks_customers",
      joinColumns = @JoinColumn(name = "id_customer"),
      inverseJoinColumns = @JoinColumn(name = "id_stock"))
  private Set<Stock> stocks = Collections.emptySet();

  public Customer() {}

  public Customer(String name, String surname, String login, String password, Set<Stock> stocks) {
    this.name = name;
    this.surname = surname;
    this.login = login;
    this.password = password;
    this.stocks = stocks;
  }

  public Long getCustomerID() {
    return customerID;
  }

  public void setCustomerID(Long customerID) {
    this.customerID = customerID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Set<Stock> getStocks() {
    return stocks;
  }

  public void setStocks(Set<Stock> stocks) {
    this.stocks = stocks;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Customer customer = (Customer) o;
    return Objects.equals(name, customer.name)
        && Objects.equals(surname, customer.surname)
        && Objects.equals(login, customer.login)
        && Objects.equals(password, customer.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, surname, login, password);
  }

    @Override
    public String toString() {
      return "Customer{" +
              ", name='" + name + '\'' +
              ", surname='" + surname + '\'' +
              '}';
    }
}
