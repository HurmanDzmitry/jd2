package by.hurman_dzmitry.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "stocks")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "stockID")
public class Stock {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id_stock")
  private Long stockID;

  @Column(name = "name")
  private String name;

  @Column(name = "bid")
  private Double bid;

  @Column(name = "number_free_stock")
  private Long numberFreeStock;

  @JsonBackReference
  @ManyToMany(mappedBy = "stocks", fetch = FetchType.EAGER)
  private Set<Customer> customers = Collections.emptySet();

  public Stock() {}

  public Stock(String name, Double bid, Long numberFreeStock, Set<Customer> customers) {
    this.name = name;
    this.bid = bid;
    this.numberFreeStock = numberFreeStock;
    this.customers = customers;
  }

  public Long getStockID() {
    return stockID;
  }

  public void setStockID(Long stockID) {
    this.stockID = stockID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getBid() {
    return bid;
  }

  public void setBid(Double bid) {
    this.bid = bid;
  }

  public Long getNumberFreeStock() {
    return numberFreeStock;
  }

  public void setNumberFreeStock(Long numberFreeStock) {
    this.numberFreeStock = numberFreeStock;
  }

  public Set<Customer> getCustomers() {
    return customers;
  }

  public void setCustomers(Set<Customer> customers) {
    this.customers = customers;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Stock stock = (Stock) o;
    return Objects.equals(name, stock.name)
        && Objects.equals(bid, stock.bid)
        && Objects.equals(numberFreeStock, stock.numberFreeStock);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, bid, numberFreeStock);
  }

  @Override
  public String toString() {
    return "Stock{" +
            ", name='" + name + '\'' +
            ", bid=" + bid +
            '}';
  }
}
