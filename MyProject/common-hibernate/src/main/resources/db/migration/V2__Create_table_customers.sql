create table customers
(
  id_customer int          not null auto_increment primary key,
  name        varchar(255) not null,
  surname     varchar(255) not null,
  login       varchar(255) not null,
  password    varchar(255) not null
);