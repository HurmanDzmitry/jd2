package by.hurman_dzmitry.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Stock.class)
public abstract class Stock_ {

	public static volatile SingularAttribute<Stock, Long> numberFreeStock;
	public static volatile SingularAttribute<Stock, Long> stockID;
	public static volatile SingularAttribute<Stock, String> name;
	public static volatile ListAttribute<Stock, Customer> customers;
	public static volatile SingularAttribute<Stock, Double> bid;

}

