insert into stocks_customers
  (id_stock, id_customer, number_bought_stock)
values ((select id_stock from stocks where name = 'Apple'), (select id_customer from customers where name = 'Dima'),10),
       ((select id_stock from stocks where name = 'GazProm'), (select id_customer from customers where name = 'Dima'),100),
       ((select id_stock from stocks where name = 'GazProm'), (select id_customer from customers where name = 'Vova'),50),
       ((select id_stock from stocks where name = 'MAZ'), (select id_customer from customers where name = 'Vova'),100),
       ((select id_stock from stocks where name = 'Boeing'), (select id_customer from customers where name = 'Ivan'),3);