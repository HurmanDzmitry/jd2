package by.hurman_dzmitry.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

public class Stock {
    private Long stockID;
    private String name;
    private Double bid;
    private Long numberFreeStock;

    public Stock() {
    }

    public Stock(Long stockID, String name, Double bid, Long numberFreeStock) {
        this.stockID = stockID;
        this.name = name;
        this.bid = bid;
        this.numberFreeStock = numberFreeStock;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBid() {
        return bid;
    }

    public void setBid(Double bid) {
        this.bid = bid;
    }

    public Long getNumberFreeStock() {
        return numberFreeStock;
    }

    public void setNumberFreeStock(Long numberFreeStock) {
        this.numberFreeStock = numberFreeStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return Objects.equals(stockID, stock.stockID) &&
                Objects.equals(name, stock.name) &&
                Objects.equals(bid, stock.bid) &&
                Objects.equals(numberFreeStock, stock.numberFreeStock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockID, name, bid, numberFreeStock);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
