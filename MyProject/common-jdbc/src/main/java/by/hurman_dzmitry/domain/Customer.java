package by.hurman_dzmitry.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;

public class Customer {
    private Long customerID;
    private String name;
    private String surname;
    private Long stockID;
    private Long numberBoughtStock;

    private String login;
    private String password;

    public Customer() {
    }

    public Customer(Long customerID, String name, String surname, Long stockID, Long numberBoughtStock, String login, String password) {
        this.customerID = customerID;
        this.name = name;
        this.surname = surname;
        this.stockID = stockID;
        this.numberBoughtStock = numberBoughtStock;
        this.login = login;
        this.password = password;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getStockID() {
        return stockID;
    }

    public void setStockID(Long stockID) {
        this.stockID = stockID;
    }

    public Long getNumberBoughtStock() {
        return numberBoughtStock;
    }

    public void setNumberBoughtStock(Long numberBoughtStock) {
        this.numberBoughtStock = numberBoughtStock;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(customerID, customer.customerID) &&
                Objects.equals(name, customer.name) &&
                Objects.equals(surname, customer.surname) &&
                Objects.equals(stockID, customer.stockID) &&
                Objects.equals(numberBoughtStock, customer.numberBoughtStock) &&
                Objects.equals(login, customer.login) &&
                Objects.equals(password, customer.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID, name, surname, stockID, numberBoughtStock, login, password);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
