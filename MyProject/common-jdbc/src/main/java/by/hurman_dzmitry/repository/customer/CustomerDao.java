package by.hurman_dzmitry.repository.customer;

import by.hurman_dzmitry.domain.Customer;
import by.hurman_dzmitry.repository.GenericDao;

import java.util.List;

public interface CustomerDao extends GenericDao<Customer, Long> {
    List<Long> batchUpdate(List<Customer> customers);

    Customer findByLogin(String login);

    List<Customer> search(String query, Integer limit, Integer offset);
}
