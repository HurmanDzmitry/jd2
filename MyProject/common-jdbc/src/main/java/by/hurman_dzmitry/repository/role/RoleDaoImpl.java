package by.hurman_dzmitry.repository.role;

import by.hurman_dzmitry.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional
public class RoleDaoImpl implements RoleDao {

    public static final String ROLE_ID = "id";
    public static final String ROLE_NAME = "name";
    public static final String ROLE_CUSTOMER_ID = "id_customer";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private Role getRoleRowMapper(ResultSet resultSet, int i) throws SQLException {
        Role role = new Role();
        role.setRoleId(resultSet.getLong(ROLE_ID));
        role.setRoleName(resultSet.getString(ROLE_NAME));
        role.setCustomerId(resultSet.getLong(ROLE_CUSTOMER_ID));
        return role;
    }

    @Override
    public List<Role> getRolesByCustomerId(Long customerId) {
        final String getRolesByCustomerId = "select * from roles where id_customer = ?";
        return jdbcTemplate.query(getRolesByCustomerId, new Object[]{customerId}, this::getRoleRowMapper);
    }

    @Override
    public List<Role> findAll() {
        final String findAllQuery = "select * from roles";
        return namedParameterJdbcTemplate.query(findAllQuery, this::getRoleRowMapper);
    }

    @Override
    public Role findById(Long id) {
        final String findById = "select * from roles where id = :roleId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("roleId", id);
        return namedParameterJdbcTemplate.queryForObject(findById, params, this::getRoleRowMapper);
    }

    @Override
    public void delete(Long id) {
        final String delete = "delete from roles where id = :roleId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("roleId", id);
        namedParameterJdbcTemplate.update(delete, params);
    }

    @Override
    public Role add(Role role) {
        final String addQuery = "INSERT INTO roles (name, id_customer) " +
                "VALUES (:roleName, :customerId);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("roleId", role.getRoleId());
        params.addValue("roleName", role.getRoleName());
        params.addValue("customerId", role.getCustomerId());
        namedParameterJdbcTemplate.update(addQuery, params, keyHolder);
        long createdRoleId = Objects.requireNonNull(keyHolder.getKey()).longValue();
        return findById(createdRoleId);
    }

    @Override
    public Role update(Role role) {
        final String createQuery = "UPDATE roles set name = :roleName, id_customer = :customerId where id = :roleId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("roleId", role.getRoleId());
        params.addValue("roleName", role.getRoleName());
        params.addValue("customerId", role.getCustomerId());
        namedParameterJdbcTemplate.update(createQuery, params);
        return findById(role.getRoleId());
    }
}
