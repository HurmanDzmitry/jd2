package by.hurman_dzmitry.repository.customer;

import by.hurman_dzmitry.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    private static final String CUSTOMER_ID = "id";
    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String STOCK_ID = "id_stock";
    private static final String NUMBER_BOUGHT_STOCK = "number_bought_stock";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private Customer getCustomerRowMapper(ResultSet resultSet, int i) throws SQLException {
        Customer customer = new Customer(resultSet.getLong(CUSTOMER_ID),
                resultSet.getString(NAME),
                resultSet.getString(SURNAME),
                resultSet.getLong(STOCK_ID),
                resultSet.getLong(NUMBER_BOUGHT_STOCK),
                resultSet.getString(LOGIN),
                resultSet.getString(PASSWORD));
        return customer;
    }

    @Override
    public List findAll() {
        final String findAllQuery = "select * from customers";
        return namedParameterJdbcTemplate.query(findAllQuery, this::getCustomerRowMapper);
    }

    @Override
    public Customer findById(Long id) {
        final String findById = "select * from customers where id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        return namedParameterJdbcTemplate.queryForObject(findById, params, this::getCustomerRowMapper);
    }

    @Override
    public void delete(Long id) {
        final String delete = "delete from customers where id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        namedParameterJdbcTemplate.update(delete, params);
    }

    @Override
    public Customer add(Customer customer) {
        final String addQuery = "INSERT INTO customers (name, surname, id_stock, number_bought_stock, login, password) " +
                "VALUES (:name,:surname, :stockId, :numberBoughtStock, :login, :password);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(addQuery, mapSqlParameterSource(customer), keyHolder);
        long createdCustomerId = Objects.requireNonNull(keyHolder.getKey()).longValue();
        return findById(createdCustomerId);
    }

    @Override
    public Customer update(Customer customer) {
        final String createQuery = "UPDATE customers set name = :name, surname = :surname, id_stock = :stockId, " +
                "number_bought_stock = :numberBoughtStock, login = :login, password = :password where id = :id";
        namedParameterJdbcTemplate.update(createQuery, mapSqlParameterSource(customer));
        return findById(customer.getCustomerID());
    }

    @Override
    public List<Long> batchUpdate(List<Customer> customers) {
        final String createQuery = "UPDATE customers set name = :name, surname = :surname, id_stock = :stockId,  " +
                "number_bought_stock = :numberBoughtStock, login = :login, password = :password where id = :id";
        List<SqlParameterSource> batch = new ArrayList<>();
        for (Customer customer : customers) {
            batch.add(mapSqlParameterSource(customer));
        }
        namedParameterJdbcTemplate.batchUpdate(createQuery, batch.toArray(new SqlParameterSource[batch.size()]));
        return customers.stream().map(Customer::getCustomerID).collect(Collectors.toList());
    }

    @Override
    public Customer findByLogin(String login) {
        final String findById = "select * from user where login = :login";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("login", login);
        return namedParameterJdbcTemplate.queryForObject(findById, params, this::getCustomerRowMapper);
    }

    @Override
    public List<Customer> search(String query, Integer limit, Integer offset) {
        final String searchQuery = "select * from CUSTOMERS where lower(name) LIKE lower(:query) or " +
                "lower(surname) LIKE lower(:query) limit :lim offset :off";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("query", "%" + query + "%");
        params.addValue("lim", limit);
        params.addValue("off", offset);
        return namedParameterJdbcTemplate.query(searchQuery, params, this::getCustomerRowMapper);
    }

    private MapSqlParameterSource mapSqlParameterSource(Customer customer) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", customer.getName());
        params.addValue("surname", customer.getSurname());
        params.addValue("stockId", customer.getStockID());
        params.addValue("numberBoughtStock", customer.getNumberBoughtStock());
        params.addValue("id", customer.getCustomerID());
        params.addValue("login", customer.getLogin());
        params.addValue("password", customer.getPassword());
        return params;
    }
}
