package by.hurman_dzmitry.repository.role;

import by.hurman_dzmitry.domain.Role;
import by.hurman_dzmitry.repository.GenericDao;

import java.util.List;

public interface RoleDao extends GenericDao<Role, Long> {
    List<Role> getRolesByCustomerId(Long userId);
}
