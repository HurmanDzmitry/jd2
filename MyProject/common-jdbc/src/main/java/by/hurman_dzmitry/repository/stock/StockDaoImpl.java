package by.hurman_dzmitry.repository.stock;

import by.hurman_dzmitry.domain.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class StockDaoImpl implements StockDao {

    private static final String STOCK_ID = "id_stock";
    private static final String NAME = "name";
    private static final String BID = "bid";
    private static final String NUMBER_FREE_STOCK = "number_free_stock";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private Stock getStockRowMapper(ResultSet resultSet, int i) throws SQLException {
        Stock stock = new Stock(resultSet.getLong(STOCK_ID),
                resultSet.getString(NAME),
                resultSet.getDouble(BID),
                resultSet.getLong(NUMBER_FREE_STOCK));
        return stock;
    }

    @Override
    public List<Stock> findAll() {
        final String findAllQuery = "select * from stocks";
        return namedParameterJdbcTemplate.query(findAllQuery, this::getStockRowMapper);
    }

    @Override
    public Stock findById(Long id) {
        final String findById = "select * from stocks where id_stock = :stockId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("stockId", id);
        return namedParameterJdbcTemplate.queryForObject(findById, params, this::getStockRowMapper);
    }

    @Override
    public void delete(Long id) {
        final String delete = "delete from stocks where id_stock = :stockId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("stockId", id);
        namedParameterJdbcTemplate.update(delete, params);
    }

    @Override
    public Stock add(Stock stock) {
        final String addQuery = "INSERT INTO stocks (name, bid, number_free_stock) " +
                "VALUES (:name, :bid, :numberFreeStock);";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(addQuery, mapSqlParameterSource(stock), keyHolder);
        long createdStockId = Objects.requireNonNull(keyHolder.getKey()).intValue();
        return findById(createdStockId);
    }

    @Override
    public Stock update(Stock stock) {
        final String updateQuery = "UPDATE stocks set name = :name, bid = :bid, " +
                "number_free_stock = :numberFreeStock where id_stock = :stockId";
        namedParameterJdbcTemplate.update(updateQuery, mapSqlParameterSource(stock));
        return findById(stock.getStockID());
    }

    @Override
    public List<Long> batchUpdate(List<Stock> stocks) {
        final String createQuery = "UPDATE stocks set name = :name, bid = :bid, " +
                "number_free_stock = :numberFreeStock where id_stock = :stockId";
        List<SqlParameterSource> batch = new ArrayList<>();
        for (Stock stock : stocks) {
            batch.add(mapSqlParameterSource(stock));
        }
        namedParameterJdbcTemplate.batchUpdate(createQuery, batch.toArray(new SqlParameterSource[batch.size()]));
        return stocks.stream().map(Stock::getStockID).collect(Collectors.toList());
    }

    @Override
    public List<Stock> search(String query) {
        final String searchQuery = "select * from STOCKS where lower(name) LIKE lower(:query)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("query", "%" + query + "%");
        return namedParameterJdbcTemplate.query(searchQuery, params, this::getStockRowMapper);
    }

    private MapSqlParameterSource mapSqlParameterSource(Stock stock) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", stock.getName());
        params.addValue("bid", stock.getBid());
        params.addValue("numberFreeStock", stock.getNumberFreeStock());
        params.addValue("stockId", stock.getStockID());
        return params;
    }
}