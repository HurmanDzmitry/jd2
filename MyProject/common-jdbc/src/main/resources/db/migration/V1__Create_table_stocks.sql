create table stocks
(
  id_stock          long         not null auto_increment primary key,
  name              varchar(255) not null,
  bid               double       not null,
  number_free_stock long
);