create table roles
(
  id          long         not null auto_increment primary key,
  name        varchar(255) not null,
  id_customer long         not null,
  constraint roles_customers_id_fk
    foreign key (id_customer) references customers (id)
      on update cascade on delete cascade
);