create table customers
(
  id                  long         not null auto_increment primary key,
  name                varchar(255) not null,
  surname             varchar(255) not null,
  id_stock            long         not null,
  constraint customers_stocks_id_stock_fk
    foreign key (id_stock) references stocks (id_stock)
      on update cascade on delete cascade,
  number_bought_stock long,
  login               varchar(255) not null,
  password            varchar(255) not null
);