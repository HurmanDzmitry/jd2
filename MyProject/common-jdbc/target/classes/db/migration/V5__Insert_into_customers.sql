insert into customers
  (name, surname, id_stock, number_bought_stock, login, password)
values ('Dima', 'Hurman', (select id_stock from stocks where name = 'Apple'), 10, 'dima', '1234'),
       ('Ivan', 'Ivanov', (select id_stock from stocks where name = 'Boeing'), 100, 'vanya', '1357'),
       ('Vova', 'Putin', (select id_stock from stocks where name = 'GazProm'), 300, 'vovick', 'qwerty');